import { useState } from "react";
import { Login } from "./components/Login/Login";
import { Muestrario } from "./components/Muestrario/Muestrario";
import { Navbar } from "./components/Navbar/Navbar";
import { ThemeContext } from "./store/ThemeContext";

const App = () => {
  const [theme, setTheme] = useState("primario");

  const toggleTheme = () => {
    setTheme(theme === "primario" ? "secundario" : "primario");
  };

  const style = `App ${theme}`;

  const logged = false;

  return (
    <ThemeContext.Provider value={{ theme, toggleTheme }}>
      <div className={style}>
        <Navbar />
        <Login />
        {logged && (
          <>
            <Muestrario />
            <Muestrario />
            <Muestrario />
            <Muestrario />
          </>
        )}
      </div>
    </ThemeContext.Provider>
  );
};

export default App;
