import { FC } from "react";

type FieldType = {
  name: string;
  masked?: boolean;
};

export const Field: FC<FieldType> = ({ name, masked }) => {
  return (
    <>
      <div>
        <label>{name}</label>
      </div>
      <div>
        <input type={`${masked ? "password" : "text"}`} />
      </div>
    </>
  );
};
