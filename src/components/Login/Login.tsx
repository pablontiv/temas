import { Field } from "../Field/Field";
import "./Login.scss";

export const Login = () => {
  return (
    <div className="login">
      <Field name="Login" />
      <Field name="Password" masked />
      <button>Log in</button>
    </div>
  );
};
