import { ThemeContext } from "../../store/ThemeContext";
import "./Navbar.scss";

export const Navbar = () => {
  return (
    <ThemeContext.Consumer>
      {({ theme, toggleTheme }) => (
        <nav>
          <ul className="navbar">
            <li>
              <button onClick={toggleTheme}>Tema Claro</button>
            </li>
            <li>
              <button onClick={toggleTheme}>Tema Oscuro</button>
            </li>
          </ul>
        </nav>
      )}
    </ThemeContext.Consumer>
  );
};

export default Navbar;
